// Soal 1
function teriak(){
	return '"Halo Sanbers!"'
}
console.log("--- SOAL 1 ---")
console.log(teriak())

// Soal 2
function kalikan(nilai1, nilai2){
	return nilai1*nilai2
}
console.log("\n--- SOAL 2 ---")
var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

// Soal 3
function introduce(name, age, address, hobby) {
	return '"Nama saya ' + name + ", umur saya " + String(age) + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + '!"'
}
console.log("\n--- SOAL 3 ---")
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)