// Soal 1
function arrayToObject(arr) {
	var now = new Date()
	var thisYear = now.getFullYear()
	var counter = 1
	if (arr.length > 0){
		for (var i=0; i < arr.length; i++){
			var newObject = {}
			newObject.firstName = arr[i][0]
			newObject.lastName = arr[i][1]
			newObject.gender = arr[i][2]
			if (arr[i][3] && arr[i][3] < thisYear){
				newObject.age = thisYear - arr[i][3]
			}else{
				newObject.age = "Invalid Birth Year"
			}
			// console.log(newObject)
			console.log(String(counter)+".", newObject.firstName, newObject.lastName, ":", newObject)
			counter += 1
		}
	}else{
		console.log('""\n')
	}
	
}
console.log("--- SOAL 1 ---")
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
console.log()
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
console.log()
// Error case 
arrayToObject([]) // ""

// Soal 2
function shoppingTime(memberId, money) {
	var tokoX = [["Sepatu Stacattu", 1500000], ["Baju Zoro", 500000], ["Baju H&N", 250000], ["Sweater Uniklooh", 175000], ["Casing Handphone", 50000]]
	if (memberId && money >= 50000){
		var objectShopping = {}
		var arrayPurchased = []
		tokoX.sort(function(a,b){ return b[1] - a[1]})
		objectShopping.memberId = memberId
		objectShopping.money = money
		for (var i=0; i<tokoX.length; i++){
			if (money >= tokoX[i][1]){
				arrayPurchased.push(tokoX[i][0])
				money -= tokoX[i][1]
			}
		}
		objectShopping.listPurchased = arrayPurchased
		objectShopping.changeMoney = money
		return objectShopping
	}else if ((memberId && money < 50000) || (!memberId && money < 50000)){
		return "Mohon maaf, uang tidak cukup"
	}else{
		return "Mohon maaf, toko X hanya berlaku untuk member saja"
	}
}
console.log("\n--- SOAL 2 ---")
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('324193hDew2', 700000));

// Soal 3
function naikAngkot(arrPenumpang) {
	rute = ['A', 'B', 'C', 'D', 'E', 'F'];
	if (arrPenumpang.length > 0){
		var newArray = []
		for (var i=0; i<arrPenumpang.length; i++){
			var objectPenumpang = {}
			var bayar = 0
			if ((arrPenumpang[i][1] > arrPenumpang[i][2]) == false){ // case, ex: 'A' > 'B'
				j = 0
				while (rute.indexOf(arrPenumpang[i][1])+j != rute.indexOf(arrPenumpang[i][2])){
					bayar += 2000
					j += 1
				}
				objectPenumpang.penumpang = arrPenumpang[i][0]
				objectPenumpang.naikDari = arrPenumpang[i][1]
				objectPenumpang.tujuan = arrPenumpang[i][2]
				objectPenumpang.bayar = bayar
				newArray.push(objectPenumpang)
			}else{ // case, ex: 'F' > 'B'
				j = 0
				while (rute.indexOf(arrPenumpang[i][2])+j != rute.indexOf(arrPenumpang[i][1])){
					bayar += 2000
					j += 1
				}
				objectPenumpang.penumpang = arrPenumpang[i][0]
				objectPenumpang.naikDari = arrPenumpang[i][1]
				objectPenumpang.tujuan = arrPenumpang[i][2]
				objectPenumpang.bayar = -1*bayar
				newArray.push(objectPenumpang)
			}
		}
		return newArray
	}else{
		return []
	}
}
console.log("\n--- SOAL 3 ---")
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
console.log()
console.log(naikAngkot([])); //[]
console.log()
console.log(naikAngkot([['Kurumi', 'D', 'A'], ['Dhina', 'F', 'B'], ['Hana', 'C', 'E']]));