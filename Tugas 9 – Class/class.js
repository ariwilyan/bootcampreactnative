// Soal 1
console.log("=== SOAL 1 (Animal Class) ===")
console.log("\nRelease 0")
class Animal {
	constructor(name) {
		this._name = name
		this._legs = 4
		this._cold_blooded = false
	}

	get name() {
		return this._name
	}

	set name(name){
		this._name = name
	}

	get legs() {
		return this._legs
	}

	set legs(legs) {
		this._legs = legs
	}

	get cold_blooded() {
		return this._cold_blooded
	}

	set cold_blooded(cold_blooded) {
		this._cold_blooded = cold_blooded
	}
}
var sheep = new Animal("shaun")

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("\nRelease 1")
class Frog extends Animal {
	constructor(name) {
		super(name)
	}

	jump() {
		console.log("hop hop")
	}
}
class Ape extends Animal {
	constructor(name) {
		super(name)
		this._legs = 2
	}

	yell() {
		console.log("Auooo")
	}
}
console.log("--- Ape ---")
var sungokong = new Ape("kera sakti")
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)
sungokong.yell() // "Auooo"

console.log("\n--- Frog ---")
var kodok = new Frog("buduk")
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)
kodok.jump() // "hop hop"

// Soal 2
console.log("\n=== SOAL 2 (Function to Class) ===")
class Clock {
	constructor({template}) {
		this._template = template
	}

	render() {
		var date = new Date()

		var hours = date.getHours()
		if (hours < 10) hours = '0' + hours

		var mins = date.getMinutes()
		if (mins < 10) mins = '0' + mins

		var secs = date.getSeconds()
		if (secs < 10) secs = '0' + secs

		var output = this._template
			.replace('h', hours)
			.replace('m', mins)
			.replace('s', secs)

		console.log(output)
	}

	stop() {
		clearInterval(this.timer)
	}

	start() {
		this.timer = setInterval(() => this.render(), 1000)
	}
}
var clock = new Clock({ template: 'h:m:s' })
clock.start()