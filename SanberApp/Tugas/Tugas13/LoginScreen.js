import React, { useEffect } from 'react'
import {
    Platform,
    View,
    Text,
    Image,
    ScrollView,
    StyleSheet,
    FlatList,
    TextInput,
    TouchableOpacity,
    Button,
    KeyboardAvoidingView
} from 'react-native'

const LoginScreen = () => {
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            style={styles.container}
        >
            <ScrollView>
                <View
                    style={styles.containerView}
                >
                    <Image
                        style={styles.logo}
                        source={require('./assets/logo.png')}
                    />
                    <Text style={styles.LoginText}>LOGIN</Text>
                    <View style={styles.FormInput}>
                        <Text style={styles.FormText}>Username</Text>
                        <TextInput style={styles.Input} />
                    </View>
                    <View style={styles.FormInput}>
                        <Text style={styles.FormText}>Password</Text>
                        <TextInput style={styles.Input} secureTextEntry={true} />
                    </View>
                </View>
                <View style={styles.kotaklogin}>
                    <TouchableOpacity style={styles.ButtonRegister}>
                        <Text style={{color: 'white', fontSize: 20}}>Daftar</Text>
                    </TouchableOpacity>
                    <Text style={styles.autotext}>Atau</Text>
                    <TouchableOpacity style={styles.ButtonLogin}>
                        <Text style={styles.TextButton}>Login</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    )
}
export default LoginScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        width: 300,
        height: 40,
        alignSelf: 'center',
        marginTop: 70,
    },
    LoginText: {
        fontSize: 36,
        padding: 18,
        textAlign: 'center',
        color: '#000000',
        marginBottom: 10
    },
    FormText: {
        color: '#000000',
        fontWeight: 'bold'
    },
    autotext: {
        fontSize: 18,
        color: '#000000',
        textAlign: 'center',
        margin: 5,
    },
    FormInput: {
        marginHorizontal: 50,
        marginVertical: 6,
        alignContent: 'center',
        width: 270,
        margin: 25,
    },
    Input: {
        height: 50,
        borderColor: '#000000',
        borderWidth: 2,
        borderRadius: 10,
    },
    ButtonLogin: {
        alignItems: 'center',
        borderColor: '#000000',
        borderWidth: 2,
        padding: 7,
        borderRadius: 10,
        marginHorizontal: 25,
        marginVertical: 10,
        marginBottom: 5,
        width: 140,
        height: 40
    },
    ButtonRegister: {
        alignItems: 'center',
        backgroundColor: '#003366',
        padding: 7,
        borderRadius: 10,
        marginHorizontal: 25,
        marginVertical: 10,
        width: 140,
        height: 40
    },
    kotaklogin: {
        alignItems: 'center',
        marginTop: 25
    },
    TextButton: {
        color: '#3EC6FF',
        fontSize: 20
    }
});