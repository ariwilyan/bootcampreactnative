import React from 'react'
import { FontAwesome5 } from '@expo/vector-icons'
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TextInput,
    Button
} from 'react-native'

const AboutScreen = () => {
    return (
        <ScrollView>
            <View style={styles.container}>
                <Text style={styles.judul}>Tentang Saya</Text>
                <FontAwesome5
                    size={200} solid
                    name="user-circle"
                    style={styles.icon} />
                <Text style={styles.judul}>Ari Wilyan Ramadhelza</Text>
                <Text style={styles.kerjaan}>IT System Developer</Text>
                <View style={styles.kotak}>
                    <Text style={styles.juduldalam}>Portfolio</Text>
                    <View style={styles.kotakdalam}>
                        <View>
                            <FontAwesome5 name="gitlab" color="green" size={40} style={styles.icon} />
                            <Text style={styles.textdalam}>@ariwilyan</Text>
                        </View>
                        <View>
                            <FontAwesome5 name="github" color="green" size={40} style={styles.icon} />
                            <Text style={styles.textdalam}>@ariwilyan</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.kotak}>
                    <Text style={styles.juduldalam}>Contact</Text>
                    <View style={styles.kotakdalam2}>
                        <View style={styles.kotakdalam3}>
                            <FontAwesome5 name="facebook" color="green" size={40} style={styles.icon} />
                            <View style={styles.textnama}>
                                <Text style={styles.textdalam}>@ariwilyan</Text>
                            </View>
                        </View>
                        <View style={styles.kotakdalam3}>
                            <FontAwesome5 name="instagram" color="green" size={40} style={styles.icon} />
                            <View style={styles.textnama}>
                                <Text style={styles.textdalam}>@ariwilyan</Text>
                            </View>
                        </View>
                        <View style={styles.kotakdalam3}>
                            <FontAwesome5 name="twitter" color="green" size={40} style={styles.icon} />
                            <View style={styles.textnama}>
                                <Text style={styles.textdalam}>@AriWilyan</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </ScrollView>
    )
}
export default AboutScreen

const styles = StyleSheet.create({
    container: {
        marginTop: 64,
    },
    judul: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#003366',
        textAlign: 'center'
    },
    icon: {
        textAlign: 'center',
        color: '#3EC6FF'
    },
    name: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#003366',
        textAlign: 'center'
    },
    kerjaan: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#000000',
        textAlign: 'center',
        marginBottom: 7
    },
    kotak: {
        borderColor: 'blue',
        borderRadius: 10,
        borderBottomColor: '#000',
        padding: 5,
        backgroundColor: '#EFEFEF',
        marginBottom: 9,
    },
    kotakdalam: {
        borderTopWidth: 2,
        borderTopColor: '#003366',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    kotakdalam2: {
        borderTopWidth: 2,
        borderTopColor: '#003366',
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    kotakdalam3: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 2,
    },
    juduldalam: {
        fontSize: 25,
        color: "#003366",

    },
    textdalam: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#003366',
        textAlign: "center"
    },
    input: {
        height: 40,
        borderColor: "grey",
        borderWidth: 1
    },
    textnama: {
        justifyContent: "center",
        marginLeft: 10
    }
})