import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { StatusBar } from 'expo-status-bar';

import Telegram from './Tugas/Tugas12/Telegram';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';
import RestApi from './Tugas/Tugas14/RestApi';
import Login from './Tugas/Tugas15/Pages/Login.js';
import Tugas15 from './Tugas/Tugas15/index.js';

import Index from './Tugas/Quiz3/index';

export default function App() {
  return (
    // <Telegram />
    // <LoginScreen />
    // <AboutScreen />
    // <RestApi />
    // <Tugas15 />
    <Index />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  }
})