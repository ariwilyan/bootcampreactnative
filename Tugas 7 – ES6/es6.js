// Soal 1
console.log("=== SOAL 1 ===")
// Mengubah fungsi menjadi fungsi arrow
const golden = function goldenFunction(){
	console.log("this is golden!!")
}
console.log("--- versi ES5 ---")
golden()
// versi arrow function
const golden2 = () => {
	console.log("this is golden with arrow function!!")
}
console.log("--- versi ES6 ---")
golden2()

// Soal 2
console.log("\n=== SOAL 2 ===")
// Sederhanakan menjadi Object literal di ES6
const newFunction = function literal(firstName, lastName){
	return {
		firstName: firstName,
		lastName: lastName,
		fullName: function(){
			console.log(firstName + " " + lastName)
			return
		}
	}
}
// versi ES6
const newFunction2 = literal = (firstName, lastName) => {
	return {
		// fullName = {firstName, lastName}
		fullName: () => {
			console.log(firstName, lastName)
		}
	}
}
 
//Driver Code
console.log("--- versi ES5 ---")
newFunction("William", "Imoh").fullName()
console.log("--- versi ES6 ---")
newFunction2("William", "Imoh").fullName()

// Soal 3
console.log("\n=== SOAL 3 ===")
// Destructuring
const newObject = {
	firstName: "Harry",
	lastName: "Potter Holt",
	destination: "Hogwarts React Conf",
	occupation: "Deve-wizard Avocado",
	spell: "Vimulus Renderus!!!"
}
// versi ES5
const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;
// versi ES6
const newObject2 = {
	firstName2: "Harry",
	lastName2: "Potter Holt",
	destination2: "Hogwarts React Conf",
	occupation2: "Deve-wizard Avocado",
	spell2: "Vimulus Renderus!!!"
}
const {firstName2, lastName2, destination2, occupation2, spell2} = newObject2
// Driver code
console.log("--- versi ES5 ---")
console.log(firstName, lastName, destination, occupation)
console.log("--- versi ES6 ---")
console.log(firstName2, lastName2, destination2, occupation2, spell2)

// Soal 4
console.log("\n=== SOAL 4 ===")
// Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// versi ES5
const combined = west.concat(east)
// versi ES6
const combined2 = [...west, ...east]
//Driver Code
console.log("--- versi ES5 ---")
console.log(combined)
console.log("--- versi ES6 ---")
console.log(combined2)

// Soal 5
console.log("\n=== SOAL 5 ===")
// Template literals
const planet = "earth"
const view = "glass"
// versi ES5
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
// versi ES6
var before2 = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
// Driver Code
console.log("--- versi ES5 ---")
console.log(before)
console.log("--- versi ES6 ---")
console.log(before2)