// Soal 1
console.log("--- SOAL 1 ---")
console.log("LOOPING PERTAMA")
i = 1
batas = 20
while (i <= batas){
	if (i % 2 == 0){
		console.log(i + " - I love coding")
	}
	i += 1
}
console.log("LOOPING KEDUA")
while (batas >= 1){
	if (batas % 2 == 0){
		console.log(batas + " - I will become a mobile developer")
	}
	batas -= 1
}

// Soal 2
console.log("\n--- SOAL 2 ---")
for (i=1; i<=20; i++){
	if (i % 3 == 0 && i % 2 == 1){
		console.log(i + " - I Love Coding")
	}else if (i % 2 == 0){
		console.log(i + " - Berkualitas")
	}else{
		console.log(i + " - Santai")
	}
}

// Soal 3
console.log("\n--- SOAL 3 ---")
for (i=0; i<4; i++){
	pagar = ""
	for (j=0; j<8; j++){
		pagar += "#"
	}
	console.log(pagar)
}

// Soal 4
console.log("\n--- SOAL 4 ---")
i = 0
while (i < 8){
	j = 0
	pagar = ""
	while (j < i){
		pagar += "#"
		j += 1
	}
	console.log(pagar)
	i += 1
}

// Soal 5
console.log("\n--- SOAL 5 ---")
for (i=0; i<8; i++){
	pagar = ""
	for (j=0; j<8; j++){
		if (i % 2 == 1 && j % 2 == 0){
			pagar += "#"
		}else if (i % 2 == 0 && j % 2 == 1){
			pagar += "#"
		}else{
			pagar += " "
		}
	}
	console.log(pagar)
}