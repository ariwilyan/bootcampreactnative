let readBooksPromise = require('./promise.js')

let books = [
	{name: 'LOTR', timeSpent: 3000},
	{name: 'Fidas', timeSpent: 2000},
	{name: 'Kalkulus', timeSpent: 4000}
]

// Lanjutkan code untuk menjalankan function readBooksPromise
let totalTime = 10000
let sizeBook = books.length

// MENJALANKAN PROMISE
function readingBooks(totalTime, index, sizeBook) {
	readBooksPromise(totalTime, books[index])
		.then(function(remainingTime){
			totalTime = remainingTime
			sizeBook -= 1
			if(sizeBook > 0){
				readingBooks(totalTime, index+1, sizeBook)
			}
		})
		.catch(function(error){
			// console.log(error.message)
		})
}

readingBooks(totalTime, 0, sizeBook)