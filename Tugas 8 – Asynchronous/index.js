// di index.js
let readBooks = require('./callback.js')

let books = [
	{name: 'LOTR', timeSpent: 3000},
	{name: 'Fidas', timeSpent: 2000},
	{name: 'Kalkulus', timeSpent: 4000}
]

// Tulis code untuk memanggil function readBooks di sini
let totalTime = 10000
let sizeArray = books.length

// MENJALANKAN CALLBACK
function readingBooks(time, index, sizeArray){
	readBooks(time, books[index], function(remainingTime){
		time = remainingTime
		sizeArray -= 1
		if(sizeArray > 0){
			readingBooks(time, index+1, sizeArray)
		}
	})
}

readingBooks(totalTime, 0, sizeArray)